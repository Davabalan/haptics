Idea : Haptics and Touch
Technology stack : Android
External api's used : None

This is a prototype for helping differently abled persons to communicate 
through any hand-held device which has an actuator(vibrator) in it.

With the help of this app one can feel the morse code for every character, 
hear out the character and even send a text and convey it as sound and morse code.

To run the app, install the Haptics.apk file from the repository.
If you feel to download rightaway then click https://goo.gl/gfptP1 .

Interested to look into a detailed documentation of this project, then head to https://goo.gl/CFT2S4
