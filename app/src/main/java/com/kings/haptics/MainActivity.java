package com.kings.haptics;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private RadioButton radioButton1, radioButton2, radioButton3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        radioButton1 = (RadioButton) findViewById(R.id.rb_both);
        radioButton1.setOnClickListener(this);
        radioButton2 = (RadioButton) findViewById(R.id.rb_audio);
        radioButton2.setOnClickListener(this);
        radioButton3 = (RadioButton) findViewById(R.id.rb_touch);
        radioButton3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == radioButton1) {
            Intent intent = new Intent(getApplicationContext(), Alphabets.class);
            intent.putExtra("enable", radioButton1.getText().toString());
            startActivity(intent);
        } else if (v == radioButton2) {
            Intent intent = new Intent(getApplicationContext(), Alphabets.class);
            intent.putExtra("enable", radioButton2.getText().toString());
            startActivity(intent);
        } else if (v == radioButton3) {
            Intent intent = new Intent(getApplicationContext(), Alphabets.class);
            intent.putExtra("enable", radioButton3.getText().toString());
            startActivity(intent);
        }
    }
}