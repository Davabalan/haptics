package com.kings.haptics;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.Locale;


public class Numbers extends AppCompatActivity implements View.OnClickListener, TextToSpeech.OnInitListener {

    public Vibrator vibrator;
    String enable;
    Button b1, b2, b3, b4, b5, b6, b7, b8, b9, b0, b10, b11, b12, b13;
    String text;
    private TextToSpeech tts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers);
        tts = new TextToSpeech(this, this);
        enable = getIntent().getStringExtra("enable");
        b1 = (Button) findViewById(R.id.bt_1);
        b2 = (Button) findViewById(R.id.bt_2);
        b3 = (Button) findViewById(R.id.bt_3);
        b4 = (Button) findViewById(R.id.bt_4);
        b5 = (Button) findViewById(R.id.bt_5);
        b6 = (Button) findViewById(R.id.bt_6);
        b7 = (Button) findViewById(R.id.bt_7);
        b8 = (Button) findViewById(R.id.bt_8);
        b9 = (Button) findViewById(R.id.bt_9);
        b0 = (Button) findViewById(R.id.bt_0);
        b10 = (Button) findViewById(R.id.bt_alpha);
        b11 = (Button) findViewById(R.id.bt_11);
        b12 = (Button) findViewById(R.id.bt_12);
        b13 = (Button) findViewById(R.id.bt_13);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        b5.setOnClickListener(this);
        b6.setOnClickListener(this);
        b7.setOnClickListener(this);
        b8.setOnClickListener(this);
        b9.setOnClickListener(this);
        b10.setOnClickListener(this);
        b0.setOnClickListener(this);
        b11.setOnClickListener(this);
        b12.setOnClickListener(this);
        b13.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == b1) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 100, 300, 400, 300, 400, 300, 400, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b1.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b1.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 100, 300, 400, 300, 400, 300, 400, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b2) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 100, 300, 100, 300, 400, 300, 400, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b2.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b2.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 100, 300, 100, 300, 400, 300, 400, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b3) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 100, 300, 100, 300, 100, 300, 400, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b3.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b3.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 100, 300, 100, 300, 100, 300, 400, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b4) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 100, 300, 100, 300, 100, 300, 100, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b4.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b4.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 100, 300, 100, 300, 100, 300, 100, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b5) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 100, 300, 100, 300, 100, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b5.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b5.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 100, 300, 100, 300, 100, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b6) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 400, 300, 100, 300, 100, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b6.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b6.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 400, 300, 100, 300, 100, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b7) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 400, 300, 400, 300, 100, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b7.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b7.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 400, 300, 400, 300, 100, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b8) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 400, 300, 400, 300, 400, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b8.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b8.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 400, 300, 400, 300, 400, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b9) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 400, 300, 400, 300, 400, 300, 400, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b9.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b9.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 400, 300, 400, 300, 400, 300, 400, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b0) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 100, 300, 100, 300, 100, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b0.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b0.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 100, 300, 100, 300, 100, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b10) {
            startActivity(new Intent(getApplicationContext(), Alphabets.class));
        } else if (v == b11) {
            long pattern[] = {0, 100, 300, 100, 300, 400, 300, 400, 300, 100, 300, 100, 300};
            vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(pattern, -1);
            text = b11.getText().toString();
            speakOut(text);
        } else if (v == b12) {
            long pattern[] = {0, 100, 300, 100, 300, 400, 300, 400, 300, 100, 300};
            vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(pattern, -1);
            text = b12.getText().toString();
            speakOut(text);
        } else if (v == b13) {
            long pattern[] = {0, 100, 300, 400, 300, 400, 300, 100, 300, 400, 300, 100, 300};
            vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(pattern, -1);
            text = b13.getText().toString();
            speakOut(text);
        }

    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = tts.setLanguage(Locale.US);
            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                speakOut(text);
            }
        } else {
            Log.e("TTS", "Initilization Failed!");
        }
    }

    @Override
    public void onDestroy() {
// Don't forget to shutdown tts!
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    private void speakOut(String text) {
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }
}
