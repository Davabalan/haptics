package com.kings.haptics;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;


public class Alphabets extends AppCompatActivity implements View.OnClickListener, TextToSpeech.OnInitListener {

    private final int REQ_CODE_SPEECH_INPUT = 100;
    public Vibrator vibrator;
    String text;
    Button b1, b2, b3, b4, b5, b6, b7, b8, b9, b10;
    Button b11, b12, b13, b14, b15, b16, b17, b18, b19, b20;
    Button b21, b22, b23, b24, b25, b26, b27, bts;
    String enable;
    private TextToSpeech tts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alphabets);
        enable = getIntent().getStringExtra("enable");
        tts = new TextToSpeech(this, this);
        b1 = (Button) findViewById(R.id.bt_a);
        b2 = (Button) findViewById(R.id.bt_b);
        b3 = (Button) findViewById(R.id.bt_c);
        b4 = (Button) findViewById(R.id.bt_d);
        b5 = (Button) findViewById(R.id.bt_e);
        b6 = (Button) findViewById(R.id.bt_f);
        b7 = (Button) findViewById(R.id.bt_g);
        b8 = (Button) findViewById(R.id.bt_h);
        b9 = (Button) findViewById(R.id.bt_i);
        b10 = (Button) findViewById(R.id.bt_j);
        b11 = (Button) findViewById(R.id.bt_k);
        b12 = (Button) findViewById(R.id.bt_l);
        b13 = (Button) findViewById(R.id.bt_m);
        b14 = (Button) findViewById(R.id.bt_n);
        b15 = (Button) findViewById(R.id.bt_o);
        b16 = (Button) findViewById(R.id.bt_p);
        b17 = (Button) findViewById(R.id.bt_q);
        b18 = (Button) findViewById(R.id.bt_r);
        b19 = (Button) findViewById(R.id.bt_s);
        b20 = (Button) findViewById(R.id.bt_t);
        b21 = (Button) findViewById(R.id.bt_u);
        b22 = (Button) findViewById(R.id.bt_v);
        b23 = (Button) findViewById(R.id.bt_w);
        b24 = (Button) findViewById(R.id.bt_x);
        b25 = (Button) findViewById(R.id.bt_y);
        b26 = (Button) findViewById(R.id.bt_z);
        b27 = (Button) findViewById(R.id.bt_num);
        bts = (Button) findViewById(R.id.bt_speak);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        b5.setOnClickListener(this);
        b6.setOnClickListener(this);
        b7.setOnClickListener(this);
        b8.setOnClickListener(this);
        b9.setOnClickListener(this);
        b10.setOnClickListener(this);
        b11.setOnClickListener(this);
        b12.setOnClickListener(this);
        b13.setOnClickListener(this);
        b14.setOnClickListener(this);
        b15.setOnClickListener(this);
        b16.setOnClickListener(this);
        b17.setOnClickListener(this);
        b18.setOnClickListener(this);
        b19.setOnClickListener(this);
        b20.setOnClickListener(this);
        b21.setOnClickListener(this);
        b22.setOnClickListener(this);
        b23.setOnClickListener(this);
        b24.setOnClickListener(this);
        b25.setOnClickListener(this);
        b26.setOnClickListener(this);
        b27.setOnClickListener(this);
        bts.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == b1) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 100, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b1.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b1.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 100, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b2) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 400, 300, 100, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b2.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b2.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 400, 300, 100, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b3) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 400, 300, 100, 300, 400, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b3.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b3.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 400, 300, 100, 300, 400, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b4) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 400, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b4.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b4.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 400, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b5) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b5.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b5.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b6) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 100, 300, 100, 300, 400, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b6.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b6.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 100, 300, 100, 300, 400, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b7) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 400, 300, 400, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b7.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b7.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 400, 300, 400, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b8) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 100, 300, 100, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b8.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b8.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 100, 300, 100, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b9) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b9.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b9.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b10) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 100, 300, 400, 300, 400, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b10.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b10.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 100, 300, 400, 300, 400, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b11) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 400, 300, 100, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b11.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b11.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 400, 300, 100, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b12) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 100, 300, 400, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b12.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b12.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 100, 300, 400, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b13) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 400, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b13.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b13.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 400, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b14) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 400, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b14.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b14.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 400, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b15) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 400, 300, 400, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b15.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b15.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 400, 300, 400, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b16) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 100, 300, 400, 300, 400, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b16.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b16.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 100, 300, 400, 300, 400, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b17) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 400, 300, 400, 300, 100, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b17.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b17.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 400, 300, 400, 300, 100, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b18) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 100, 300, 400, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b18.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b18.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 100, 300, 400, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b19) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 100, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b19.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b19.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 100, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b20) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b20.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b20.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b21) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 100, 300, 100, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b21.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b21.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 100, 300, 100, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b22) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 100, 300, 100, 300, 100, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b22.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b22.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 100, 300, 100, 300, 100, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b23) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 100, 300, 400, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b23.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b23.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 100, 300, 400, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b24) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 400, 300, 100, 300, 100, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b24.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b24.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 400, 300, 100, 300, 100, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b25) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 400, 300, 100, 300, 400, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b25.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b25.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 400, 300, 100, 300, 400, 300, 400, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b26) {
            if (enable.equals("Enable both")) {
                long pattern[] = {0, 400, 300, 400, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                text = b26.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Audio")) {
                text = b26.getText().toString();
                speakOut(text);
            } else if (enable.equals("Enable Touch Sense")) {
                long pattern[] = {0, 400, 300, 400, 300, 100, 300, 100, 300};
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
            }
        } else if (v == b27) {
            Intent intent = new Intent(getApplicationContext(), Numbers.class);
            intent.putExtra("enable", enable);
            startActivity(intent);
        } else if (v == bts) {
            promptSpeechInput();
        }
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = tts.setLanguage(Locale.US);
            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                speakOut(text);
            }
        } else {
            Log.e("TTS", "Initilization Failed!");
        }
    }

    @Override
    public void onDestroy() {
// Don't forget to shutdown tts!
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    private void speakOut(String text) {
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String textTemp = result.get(0);
                    Log.d("Sucess", textTemp + "");
                    //textTemp = textTemp.toLowerCase();
                    for (int i = 0; i < textTemp.length(); i++) {
                        char v = textTemp.charAt(i);
                        Log.d("dava", v + "");
                        if (v == 'a') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 100, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                Log.d("in_A", "in");
                                text = v + "";
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = v + "";
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 100, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'b') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 400, 300, 100, 300, 100, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = v + "";
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = v + "";
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 400, 300, 100, 300, 100, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'c') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 400, 300, 100, 300, 400, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = v + "";
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = v + "";
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 400, 300, 100, 300, 400, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'd') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 400, 300, 100, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = v + "";
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = v + "";
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 400, 300, 100, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'e') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b5.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b5.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'f') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 100, 300, 100, 300, 400, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b6.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b6.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 100, 300, 100, 300, 400, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'g') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 400, 300, 400, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b7.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b7.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 400, 300, 400, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'h') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 100, 300, 100, 300, 100, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b8.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b8.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 100, 300, 100, 300, 100, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'i') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 100, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b9.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b9.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 100, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'j') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 100, 300, 400, 300, 400, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b10.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b10.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 100, 300, 400, 300, 400, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'k') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 400, 300, 100, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b11.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b11.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 400, 300, 100, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'l') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 100, 300, 400, 300, 100, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b12.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b12.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 100, 300, 400, 300, 100, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'm') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 400, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b13.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b13.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 400, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'n') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 400, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b14.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b14.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 400, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'o') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 400, 300, 400, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b15.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b15.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 400, 300, 400, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'p') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 100, 300, 400, 300, 400, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b16.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b16.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 100, 300, 400, 300, 400, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'q') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 400, 300, 400, 300, 100, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b17.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b17.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 400, 300, 400, 300, 100, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'r') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 100, 300, 400, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b18.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b18.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 100, 300, 400, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 's') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 100, 300, 100, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b19.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b19.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 100, 300, 100, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 't') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b20.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b20.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'u') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 100, 300, 100, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b21.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b21.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 100, 300, 100, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'v') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 100, 300, 100, 300, 100, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b22.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b22.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 100, 300, 100, 300, 100, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'w') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 100, 300, 400, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b23.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b23.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 100, 300, 400, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'x') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 400, 300, 100, 300, 100, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b24.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b24.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 400, 300, 100, 300, 100, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'y') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 400, 300, 100, 300, 400, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b25.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b25.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 400, 300, 100, 300, 400, 300, 400, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        } else if (v == 'z') {
                            if (enable.equals("Enable both")) {
                                long pattern[] = {0, 400, 300, 400, 300, 100, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                                text = b26.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Audio")) {
                                text = b26.getText().toString();
                                speakOut(text);
                            } else if (enable.equals("Enable Touch Sense")) {
                                long pattern[] = {0, 400, 300, 400, 300, 100, 300, 100, 300};
                                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(pattern, -1);
                            }
                        }
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
